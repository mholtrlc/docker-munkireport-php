# MunkiReport Dockerfile
# Version 0.3
FROM richarvey/nginx-php-fpm:mainline

MAINTAINER Michael Holt <mholt@reallifechurch.org>

ENV MR_CLIENT_PASSPHRASE "'phrase1','phrase2'"
ENV MR_ALLOW_MIGRATIONS TRUE
ENV MR_TEMP_UNIT F
ENV MR_KEEP_PREVIOUS_DISPLAYS TRUE
ENV MR_MODULES "'munkireport','disk_report','inventory'"
ENV MR_SITE_NAME Real Life Church
ENV MR_APPS_TO_TRACK array('Safari')
ENV MR_TZ America/Los_Angeles
ENV TZ America/Los_Angeles
ENV MR_DEBUG FALSE
ADD run.sh /run.sh

#CMD ["/run.sh"] 